const express = require("express");
const path = require('path');
const uploadImage = require("./middleware/uploadImage");

const app = express();
app.use(express.json());

const dir = path.join(__dirname, 'public', 'upload');
app.use("/images", express.static(dir));

app.post("/images", uploadImage.single("image"), async (req, res) => {
  if (req.file) {
    console.log(req.file);
    return res.json({
      error: false,
      message: "Upload successful!",
    });
  }

  return res.status(404).json({
    error: true,
    message: "Upload failed!",
  });
});

app.use("/api", require("./routes/api.route"));


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`🚀 @ http://localhost:${PORT}`));
